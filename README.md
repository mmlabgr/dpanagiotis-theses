##Synopsis

This is a tool to certify and/or verify the validity of various degrees and tittles utilizing the blockchain technology and it's unique characteristics.

This implementation was developed in the frame of my thesis which was presented at the **Department of Computer Engineering & Informatics** of **University of Patras**.


Droumpalis Panagiotis,

November 2018.

####For any questions feel free to contact me at: pdroumpalis@outlook.com