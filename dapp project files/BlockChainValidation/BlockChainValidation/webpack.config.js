﻿const path = require('path');

module.exports = {
    entry: ['./Scripts/main.js'],
    output: {
        path: path.resolve(__dirname, './wwwroot/js'),
        filename: 'site.js'
    },
    // IMPORTANT NOTE: If you are using Webpack 2, replace "loaders" with "rules"
    module: {
        loaders: [
            {
                loader: 'babel-loader',
                test: /\.js$/,
                exclude: /node_modules/
            }
        ]
    }
};