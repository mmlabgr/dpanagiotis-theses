﻿using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace BlockChainValidation.Extensions
{
    public class UserRoleSeed
    {
        private readonly RoleManager<IdentityRole> _roleManager;

        public UserRoleSeed(RoleManager<IdentityRole> roleManager)
        {
            _roleManager = roleManager;
        }

        public async Task SeedAsync()
        {
            if ((!await _roleManager.RoleExistsAsync("Admin")))
            {
                await _roleManager.CreateAsync(new IdentityRole { Name = "Admin" });
            }
            if ((await _roleManager.FindByNameAsync("Moderator")) == null)
            {
                await _roleManager.CreateAsync(new IdentityRole { Name = "Moderator" });
            }
            if ((await _roleManager.FindByNameAsync("Member")) == null)
            {
                await _roleManager.CreateAsync(new IdentityRole { Name = "Member" });
            }

        }
    }
}