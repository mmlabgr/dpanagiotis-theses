﻿import { default as Web3 } from 'web3';
import { default as contract } from 'truffle-contract';
import EthDegreeValidationArtifacts from '../truffle/build/contracts/EthDegreeValidation.json';
import { handle_file_select } from '../Scripts/file_handler_v2';
import swal from 'sweetalert';

//make the truffle-contract representation of EthDegreeValidation
var EthdvContract = contract(EthDegreeValidationArtifacts);

//User's connected account (address)
var account;
var injectedAccountExists = false;

//form link to the contract on ropsten.etherscan.io
if (document.getElementById('etherscan_link')) {
    var contr_address = EthdvContract.networks["3"].address;
    var link = "https://ropsten.etherscan.io/address/" + contr_address + "#code";
    document.getElementById('etherscan_link').href = link;
}


window.App = {
    start: function () {

        //Log web3 version
        console.log("Web3 version: " + web3.version); // "1.0.0-beta.29"

        //log the contract as truffle-contract object
        //console.log(EthdvContract);


        // Bootstrap the Contact abstraction for Use.
        EthdvContract.setProvider(web3.currentProvider);
        fixTruffleContractCompatibilityIssue(EthdvContract);


        //Log provider
        console.log(EthdvContract.currentProvider);

        /*
        //display the deployed contract object on log 
        EthdvContract.deployed().then(function (instance) {
            var deplcontr = instance;
            console.log(deplcontr);
        }).catch(function (err) {
            //  catch errors along execution
            if (err.message === "EthDegreeValidation has not been deployed to detected network (network/artifact mismatch)") {
                swal("Oops! It looks like the selected network on Metamask is not the correct one...", "Switch metamask network to Ropsten!", "error");
            } else {
                swal("ERROR! ", err.message, "error");
            }
        });
        */
    }
};


window.addEventListener('load', function () {
    var isConnected = connectToEthereum();
    if (isConnected) {
        getAccounts();
        App.start();
    }
});

window.init = init;
function init() {
    var isConnected = connectToEthereum();
    if (isConnected) {
        getAccounts();
        App.start();
        swal("Success!", "Connected to Ethereum network!", "success");
    } else {
        swal("Oh, snap!", "No web3 detected. Ensure Metamask is installed and properly configured.", "error");
    }
}

function connectToEthereum() {
    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof web3 !== 'undefined') {
        console.warn("Using web3 detected from external source. If you find that your accounts don't appear, ensure you've configured that source properly.");
        // Use Mist/MetaMask's provider
        window.web3 = new Web3(web3.currentProvider);
        //update ui
        document.getElementById('connected_text').className = "text-success";
        document.getElementById('connected_text').innerHTML = "Connected to Ethereum";
        document.getElementById('provider').innerHTML = web3.currentProvider.__proto__.constructor.name;
        document.getElementById('provider').className = "text-success";
        document.getElementById('provider_p').style = "display: block";
        //check network
        networkCheck();
        return true;
    } else {
        //console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development.");
        // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
        //window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
        document.getElementById('connected_text').className = "text-danger";
        document.getElementById('connected_text').innerHTML = "Not connected to Ethereum";
        //swal("Oh, snap!", "No web3 detected. Ensure Metamask is installed and properly configured.", "error");
        return false;
    }
}

function getAccounts() {
    // Get accounts
    web3.eth.getAccounts(function (err, accs) {
        if (err !== null) {
            swal("There was an error fetching your accounts.", err.message, "error");
            injectedAccountExists = false;
            document.getElementById('account_p').style = "display: none";
            return;
        }
        if (accs.length === 0) {
            //swal("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.", "", "error");
            console.warn("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
            document.getElementById('account').innerHTML = "Log in to Metamask to display accounts.";
            document.getElementById('account').className = "text-warning";
            document.getElementById('account_p').style = "display: block";
            injectedAccountExists = false;
            return;
        }

        injectedAccountExists = true;

        //run only if account is changed
        if (account !== accs[0]) {
            account = accs[0];
            document.getElementById('account_p').style = "display: none";
            document.getElementById('account').innerHTML = account;
            EthdvContract.deployed().then(function (instance) {
                var deplcontr = instance;
                deplcontr.isMod.call(account).then(function (result) {
                    if (result) {
                        document.getElementById('account').className = "text-success";
                    } else {
                        document.getElementById('account').className = "";
                    }
                    console.log("called the contract");
                });
            }).catch(function (err) {
                //  catch errors along execution
                if (err.message === "EthDegreeValidation has not been deployed to detected network (network/artifact mismatch)") {
                    swal("Oops! It looks like the selected network on Metamask is not the correct one...", "Switch metamask network to Ropsten!", "error");
                } else {
                    swal("ERROR! ", err.message, "error");
                }
            });
            document.getElementById('account_p').style = "display: block";
        }
        //For debugging
        //console.log("ta accounts:");
        //console.log(accounts);
    });
}

function networkCheck() {
    web3.eth.net.getId().then(function (netId) {
        switch (netId) {
            case 1:
                document.getElementById('network').innerHTML = "Ethereum Mainnet";
                document.getElementById('network').className = "text-danger";
                document.getElementById('network_p').style = "display: block";
                //console.log('This is mainnet');
                break;
            case 2:
                document.getElementById('network').innerHTML = "Morden test network (deprecated)";
                document.getElementById('network').className = "text-danger";
                document.getElementById('network_p').style = "display: block";
                //console.log('This is the deprecated Morden test network.');
                break;
            case 3:
                document.getElementById('network').innerHTML = "Ropsten test network";
                document.getElementById('network').className = "text-success";
                document.getElementById('network_p').style = "display: block";
                //console.log('This is the ropsten test network.');
                break;
            case 4:
                document.getElementById('network').innerHTML = "Rinkeby test network";
                document.getElementById('network').className = "text-danger";
                document.getElementById('network_p').style = "display: block";
                //console.log('This is the Rinkeby test network.');
                break;
            case 42:
                document.getElementById('network').innerHTML = "Kovan test network";
                document.getElementById('network').className = "text-danger";
                document.getElementById('network_p').style = "display: block";
                //console.log('This is the Kovan test network.');
                break;
            default:
                document.getElementById('network').innerHTML = "Unknown network";
                document.getElementById('network').className = "text-danger";
                document.getElementById('network_p').style = "display: block";
                //console.log('This is an unknown network.');
        }
    });
}

// Workaround for a compatibility issue between web3@1.0.0-beta.29 and truffle-contract@3.0.3
// More information on this found in the following links
// https://github.com/trufflesuite/truffle-contract/issues/57#issuecomment-331300494
// https://github.com/trufflesuite/truffle-contract/issues/56
// cutting edge -_-
function fixTruffleContractCompatibilityIssue(contract) {
    if (typeof contract.currentProvider.sendAsync !== "function") {
        contract.currentProvider.sendAsync = function () {
            return contract.currentProvider.send.apply(
                contract.currentProvider, arguments
            );
        };
    }
}

window.verifyDegree = verifyDegree;
function verifyDegree() {

    //get the arguments from the form
    var sha256hash = document.getElementById('form_hash').value;
    console.log("Argument hash ready: " + sha256hash);

    //check if the arguments are valid
    var validHash = document.getElementById('hashForm').className === "form-group has-success";
    console.log("Valid Hash check" + validHash);

    if (validHash) {

        //hide past results
        document.getElementById('result_success').style = "display: none";
        document.getElementById('result_failure').style = "display: none";
        //display loading icon
        document.getElementById('loading').style = "display: block";


        EthdvContract.deployed().then(function (instance) {
            //instantiate contract
            var deplcontr = instance;
            //call getDegree
            deplcontr.getDegree.call(sha256hash).then(function (result) {
                //log results
                console.log("The returned result object is: " + result);
                var name = result[1];
                console.log("This degree belongs to: " + name);
                var date = result[2].toString();
                console.log("Time of deployment in unix timestamp form: " + date);
                //handling unix timestamp
                var readable_date = new Date(date * 1000).toUTCString();
                console.log("Time of deployment in a more readable format: " + readable_date);

                //get the event that was created when the degree was certified to get the tx_hash
                var event = deplcontr.DegreeAdded({ _hash: sha256hash }, { fromBlock: 4000000, toBlock: 'latest' });
                event.get((error, result) => {
                    // we got the event that was triggered when the degree was added
                    //console.log(result[0].args);
                    //this is the transaction hash
                    console.log(result[0].transactionHash);
                    //form link to the transaction on ropsten.etherscan.io
                    document.getElementById('tx_hash').innerHTML = result[0].transactionHash;
                    document.getElementById('tx_link').href = "https://ropsten.etherscan.io/tx/" + result[0].transactionHash;
                    //load results in the result panel
                    document.getElementById('owner').innerHTML = name;
                    document.getElementById('date').innerHTML = readable_date;
                    //hide loading icon
                    document.getElementById('loading').style = "display: none";
                    //display results panel
                    document.getElementById('result_failure').style = "display: none";
                    document.getElementById('result_success').style = "display: block";

                });
            }).catch(function (err) {
                if (err.message === "new BigNumber() not a base 16 number: ") {
                    //the call errored and returned nothing, did not get expected result so we get this error as we try to parse the result
                    // This means this sha256 is not verified
                    console.log("This file is not a verified Degree.");
                    //update ui
                    document.getElementById('loading').style = "display: none";
                    document.getElementById('result_success').style = "display: none";
                    document.getElementById('result_failure').style = "display: block";
                } else {
                    document.getElementById('loading').style = "display: none";
                    swal("ERROR! ", err.message, "error");
                }
            });
        }).catch(function (err) {
            //  catch errors along the whole execution
            document.getElementById('loading').style = "display: none";
            if (err.message === "EthDegreeValidation has not been deployed to detected network (network/artifact mismatch)") {
                swal("Oops! It looks like the selected network on Metamask is not the correct one...", "Switch metamask network to Ropsten!", "error");
            } else {
                swal("ERROR! ", err.message, "error");
            }
        });
    }
}

window.certifyDegree = certifyDegree;
function certifyDegree() {

    //get the arguments from the form
    var name = document.getElementById('owner_name').value;
    console.log("Argument name ready: " + name);
    var sha256hash = document.getElementById('form_hash').value;
    console.log("Argument hash ready: " + sha256hash);
    //check if the arguements are valid
    var validHash = document.getElementById('hashForm').className === "form-group has-success";
    console.log("Valid Hash check" + validHash);
    var validName = document.getElementById('nameForm').className === "form-group has-success";
    console.log("Valid Name check" + validName);


    if (validHash && validName) {

        //display loading icon
        document.getElementById('loading').style = "display: block";


        EthdvContract.deployed().then(function (instance) {
            //instantiate contract
            var deplcontr = instance;
            //send transaction addNewDegree function
            deplcontr.addNewDegree(name, sha256hash, { from: account, gas: 400000 }).then(function (result) {

                //log info
                console.log("Degree was certified successfully!");
                console.log(result);
                //hide loader
                document.getElementById('loading').style = "display: none";
                //show notification
                var newElement = document.createElement("div");
                newElement.className = "alert alert-success alert-dismissible fade in";
                newElement.style = "margin-left: -16px;margin-right: -17px";
                newElement.innerHTML = "<a href=\"#\" class=\"close\" data-dismiss=\"alert\">&times;</a>" + "Success! The degree belonging to " + name + " was certified successfully! " + "<p><a href=\"https://ropsten.etherscan.io/tx/" + result.tx + "\" target=\"_blank\" class=\"alert-link\">See the transaction</a></p>";
                var alerts = document.getElementById('alerts');
                alerts.insertBefore(newElement, alerts.childNodes[0]);
            }).catch(function (err) {
                swal("ERROR! ", err.message, "error");
                //log info
                console.log("Degree was not certified. The transaction erroed.");
                console.log(err);
                //hide loader
                document.getElementById('loading').style = "display: none";
                //show notification
                var newElement = document.createElement("div");
                newElement.className = "alert alert-danger alert-dismissible fade in";
                newElement.style = "margin-left: -16px;margin-right: -17px";
                newElement.innerHTML = "<a href=\"#\" class=\"close\" data-dismiss=\"alert\">&times;</a>" + "Error! The transaction regarding " + name + " errored. " + "<p><a href=\"https://ropsten.etherscan.io/tx/" + err.tx + "\" target=\"_blank\" class=\"alert-link\">See the transaction </a></p>" + "<p><a class=\"alert-link\" href=\"#tx_fail_info\" data-toggle=\"modal\">What caused this?</a></p>";
                var alerts = document.getElementById('alerts');
                alerts.insertBefore(newElement, alerts.childNodes[0]);
            });
        }).catch(function (err) {
            //  catch errors along the whole execution
            document.getElementById('loading').style = "display: none";
            if (err.message === "EthDegreeValidation has not been deployed to detected network (network/artifact mismatch)") {
                swal("Oops! It looks like the selected network on Metamask is not the correct one...", "Switch metamask network to Ropsten!", "error");
            } else {
                swal("ERROR! ", err.message, "error");
            }
            //hide loader
            document.getElementById('loading').style = "display: none";
        });
    }
}

window.handle_file_select = handle_file_select;

//function to update account when user changes account through metamask
window.detectAccountChange = detectAccountChange;
function detectAccountChange() {
    //only poll for changes if an account exists
    if (injectedAccountExists) {
        getAccounts();
        console.log("polling");
    }
}