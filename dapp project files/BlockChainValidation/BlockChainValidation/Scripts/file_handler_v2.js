﻿export function handle_file_select(event) {

    var input = event.target;
    var output;
    document.getElementById('file_name').value = event.target.files[0].name;

    var reader = new FileReader();
    reader.onload = function () {
        var arrayBuffer = reader.result;
        console.log(arrayBuffer.byteLength);
        document.getElementById('file_load_results').style = "display: block;";
        var output_str = "Bytes of selected file: " + arrayBuffer.byteLength;
        document.getElementById('file_size').innerHTML = output_str;
        document.getElementById('sha_result').innerHTML = "Calculating Hash...";


        sha256(arrayBuffer).then(function (digest) {
            console.log(digest);
            var output_str = "SHA-256 Hash: " + digest;
            document.getElementById('sha_result').innerHTML = output_str;
            document.getElementById('form_hash').value = "0x" + digest;
            document.getElementById('hashForm').className = "form-group has-success";
        });

        function sha256(buffer) {
            return crypto.subtle.digest("SHA-256", buffer).then(function (hash) {
                return hex(hash);
            });
        }

        function hex(buffer) {
            var hexCodes = [];
            var view = new DataView(buffer);
            for (var i = 0; i < view.byteLength; i += 4) {
                // Using getUint32 reduces the number of iterations needed (we process 4 bytes each time)
                var value = view.getUint32(i);
                // toString(16) will give the hex representation of the number without padding
                var stringValue = value.toString(16);
                // We use concatenation and slice for padding
                var padding = '00000000';
                var paddedValue = (padding + stringValue).slice(-padding.length);
                hexCodes.push(paddedValue);
            }

            // Join all the hex strings into one
            return hexCodes.join("");
        }

    };
    reader.onprogress = function () {

    };
    reader.readAsArrayBuffer(input.files[0]);
}