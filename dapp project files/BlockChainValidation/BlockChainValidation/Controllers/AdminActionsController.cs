﻿using BlockChainValidation.Data;
using BlockChainValidation.Models;
using BlockChainValidation.Models.AdminActionsViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BlockChainValidation.Controllers
{
    [Authorize(Roles ="Admin")]
    public class AdminActionsController : Controller
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public AdminActionsController(
            ApplicationDbContext dbContext, 
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            _dbContext = dbContext;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public IActionResult RedirectToIndex()
        {
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Index()
        {
            //creating and populating the Viewmodel with all users and their roles
            var listOfUsers = (from u in _dbContext.Users.OrderBy(u => u.Email)
                               let query = (from ur in _dbContext.UserRoles
                                            where ur.UserId.Equals(u.Id)
                                            join r in _dbContext.Roles on ur.RoleId equals r.Id select r.Name)
                               select new IndexViewModel { User = u, Roles = query.ToList<string>() })
                                             .ToList();
            return View(listOfUsers);
        }

        [HttpGet]
        public async Task<IActionResult> AddRole(string id)
        {
            var user = await GetUserByIdAsync(id);
            var vm = new AddRoleViewModel
            {
                Roles = GetAllRoles(),
                UserId = id,
                Email = user.Email
            };
            return View(vm);
        }

        [HttpPost]
        public async Task<IActionResult> AddRole(AddRoleViewModel rvm)
        {
            var user = await GetUserByIdAsync(rvm.UserId);
            if (ModelState.IsValid)
            {
                var result = await _userManager.AddToRoleAsync(user, rvm.NewRole);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(error.Code, error.Description);
                }
            }
            rvm.Roles = GetAllRoles();
            rvm.Email = user.Email;
            return View(rvm);
        }

        [HttpGet]
        public async Task<IActionResult> RemoveRole(string id)
        {
            var user = await GetUserByIdAsync(id);
            var vm = new RemoveRoleViewModel
            {
                Roles = new SelectList(await _userManager.GetRolesAsync(user)),
                UserId = id,
                Email = user.Email
            };
            return View(vm);
        }

        [HttpPost]
        public async Task<IActionResult> RemoveRole(RemoveRoleViewModel rvm)
        {
            var user = await GetUserByIdAsync(rvm.UserId);
            if (ModelState.IsValid)
            {
                var result = await _userManager.RemoveFromRoleAsync(user, rvm.RoleToBeRemoved);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(error.Code, error.Description);
                }
            }
            rvm.Roles = new SelectList(await _userManager.GetRolesAsync(user));
            rvm.Email = user.Email;
            return View(rvm);
        }

        [HttpGet]
        public async Task<IActionResult> DeleteUser(string id)
        {
            var user = await GetUserByIdAsync(id);
            var vm = new DeleteUserViewModel
            {
                UserId = id,
                Email = user.Email
            };
            return View(vm);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteUser(DeleteUserViewModel rvm)
        {
            var user = await GetUserByIdAsync(rvm.UserId);
            if (ModelState.IsValid)
            {
                var result = await _userManager.DeleteAsync(user);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(error.Code, error.Description);
                }
            }
            return View("Index");
        }

        private async Task<ApplicationUser> GetUserByIdAsync(string id)
        {
            return await _userManager.FindByIdAsync(id);
        }

        private SelectList GetAllRoles()
        {
            return new SelectList(_roleManager.Roles.OrderBy(r => r.Name));
        }
    }
}
