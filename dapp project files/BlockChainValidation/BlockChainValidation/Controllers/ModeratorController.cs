﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BlockChainValidation.Controllers
{
    [Authorize(Roles = "Admin,Moderator")]
    public class ModeratorController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Message"] = "Certify that a Degree exists by publishing it on the Etherium Blockchain";
            return View();
        }
    }
}