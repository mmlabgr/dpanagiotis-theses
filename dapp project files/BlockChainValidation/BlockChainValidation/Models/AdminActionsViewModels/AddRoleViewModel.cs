﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace BlockChainValidation.Models.AdminActionsViewModels
{
    public class AddRoleViewModel
    {
        public string UserId { get; set; }
        [Required]
        [Display(Name = "\"Role to Add\"")]
        public string NewRole { get; set; }
        public SelectList Roles { get; set; }
        public string Email { get; set; }
    }
}