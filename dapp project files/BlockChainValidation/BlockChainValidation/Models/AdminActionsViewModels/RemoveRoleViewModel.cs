﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace BlockChainValidation.Models.AdminActionsViewModels
{
    public class RemoveRoleViewModel
    {
        public string UserId { get; set; }
        [Required]
        [Display(Name = "\"Role to Remove\"")]
        public string RoleToBeRemoved { get; set; }
        public SelectList Roles { get; set; }
        public string Email { get; set; }
    }
}