﻿namespace BlockChainValidation.Models.AdminActionsViewModels
{
    public class DeleteUserViewModel
    {
        public string UserId { get; set; }
        public string Email { get; set; }
    }
}