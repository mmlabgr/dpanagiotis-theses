﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace BlockChainValidation.Models.AdminActionsViewModels
{
    public class IndexViewModel
    {
        //public List<ApplicationUser> Users { get; set; }
        // testing below this comment
        //public IList<IList<string>> Roles { get; set; }
        public ApplicationUser User { set; get; }
        public List<string> Roles { set; get; }

    }
}