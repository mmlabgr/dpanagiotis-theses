Instructions to run the application localy:
================

***

### 1. Install Microsoft Visual Studio 2017:

We suggest installing the following workloads:

+ .NET desktop development
+ ASP.NET and web development
+ Node.js development
+ Data storage and processing
+ .Net Core cross-platform development

### 2. Clone the directory on a local folder on your computer.

### 3. Open the project with visual studio using the `BlockChainValidation.sln` file

### 4. Open the file `Startup.cs` and comment out line 88

`new UserRoleSeed(serviceProvider.GetService<RoleManager<IdentityRole>>()).SeedAsync().Wait();`

This line runs the UserRoleSeed service to add the Roles on the database. This will produce an error since the database is not even populated yet.

### 5. Build Solution

You can find this option under the Build>Build Solution menu on the toolbar.

### 6. Open Package Manager Console and run the following command

`Update-Database`

You can find the Package Manager Console at Tools>NuGet Package Manager>Packet Manager Console

### 7. Now, after migrations are applied, return to `Startup.cs` file and uncomment line 88

### 8. Build the solution one more time

### 9. Launch the application by hitting the F5 key, voila!

Notes:
=========

***

**During development use [npm-watch](https://www.npmjs.com/package/npm-watch) to automatically build and re-generate the webpack bundle after every change in any script as specified in package.json.**

After installing npm-watch, open a command window, navigate to the project directory *"~/BlockChainValidation"* 
and run the following command:

`npm run watch`

The script will now watch for any changes in `*.js` files and builds the project on the fly, whenever needed


You can also manually force a rebuild using the following command:

`npm run build`

**Sendgrid API key**

Don't forget that you need a Sendgrid API key for the verify email functionality. The API key needs to be stored in an Windows environmental variable called `SENDGRID_API_KEY_2`.

If you don't want to get an API key to run test the project you can do the following:

+ Open the `Startup.cs` file.
+ Go to line 36 and change the requirement to *false*: `config.SignIn.RequireConfirmedEmail = false;`
+ Save and rebuild the solution.
